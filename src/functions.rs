use an_operation_support::{describe, Bytes};
use std::collections::BTreeMap;

async fn get_url_bytes(file_url: &String) -> Result<Bytes, reqwest::Error> {
    let client = reqwest::Client::new();
    let res = client.get(file_url).send().await?;
    let res = res.bytes().await?;
    Ok(res.to_vec())
}

/// op_file
///
/// Fetch a file from the given URL and produce its bytes as output.
///
/// The `describe` attribute allow generation of manifest
/// with the following explicit properties:
///
///  * _group_: SYS
///  * _config_: no configuration parameters
///  * _features_: only std is supported, since there is no HTTP client
///    capable of abstracting of the network interface provided by std
///
/// # Arguments
///  * file_url: the URL of the file to fetch
///  * _: (configuration) ignored
///
/// # Return
/// A [`Result`] having an Ok value of [`Bytes`] type and a Err value of [`reqwest::Error`] type.
///
/// **NOTE:** The function returns asynchronously, by mean of a [`core::future::Future`].
#[describe([
    groups = [
        "SYS",
    ],
    config = [],
    features = []
])]
pub async fn execute(
    file_url: &String,
    _: BTreeMap<String, String>,
) -> Result<Bytes, reqwest::Error> {
    let bytes = get_url_bytes(&file_url).await?;
    Ok(bytes)
}

#[cfg(test)]
mod tests {
    use crate::execute;
    use std::collections::BTreeMap;

    #[test]
    fn test_describe() {
        use an_operation_support::operation::OperationManifestData;
        use serde_json;

        let manifest_data: OperationManifestData =
            serde_json::from_str(&crate::describe()).unwrap();

        assert_eq!("op_file", manifest_data.name);
        assert_eq!(1, manifest_data.inputs.len());
        assert_eq!("String", manifest_data.inputs.get(0).unwrap());
        assert_eq!("Bytes", manifest_data.output);
        assert_eq!(0, manifest_data.features.len());
    }

    #[tokio::test]
    async fn test_execute() {
        let input = String::from("https://bafybeiavjzfgrxx2zq5r3vx352amhuzdv5pc5cu32xp7tlh4iqvcuxjcze.ipfs.dweb.link/tenerife-light-painting-01-1000x1000.jpg");

        let op_output = execute(&input, BTreeMap::new()).await;

        assert!(op_output.is_ok());
        assert!(op_output.unwrap().len() > 0)
    }
}
