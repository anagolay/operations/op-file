use crate::execute;
use an_operation_support::{from_map, from_value, to_value};
use js_sys::Map;
#[cfg(debug_assertions)]
use std::panic;
use wasm_bindgen::prelude::wasm_bindgen;
use wasm_bindgen::JsValue;

/// op_file
///
/// Fetch a file from the given URL and produce its bytes as output.
/// This is the WASM binding of the Operation `execute()` function
///
/// # Arguments
///  * operation_inputs: collection of serialized inputs
///  * config: configuration map
///
/// # Return
/// A promise that resolves to [`Bytes`] if it succeeds, or to an error message
/// [`String`] otherwise
#[wasm_bindgen(js_name=execute)]
pub async fn wasm_execute(operation_inputs: Vec<JsValue>, config: Map) -> Result<JsValue, JsValue> {
    #[cfg(debug_assertions)]
    panic::set_hook(Box::new(console_error_panic_hook::hook));

    let input: String = from_value(operation_inputs.get(0).unwrap())?;
    let config = from_map(&config.into())?;

    let output = execute(&input, config).await?;

    to_value(&output)
}

/// Output manifest data
#[wasm_bindgen(js_name=describe)]
pub fn wasm_describe() -> String {
    #[cfg(debug_assertions)]
    panic::set_hook(Box::new(console_error_panic_hook::hook));

    crate::describe()
}
